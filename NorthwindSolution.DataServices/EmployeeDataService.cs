﻿using Dapper;
using NorthwindSolution.Entities;
using NorthwindSolution.IDataServices;
using NorthWindSolution.Core;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace NorthwindSolution.DataServices
{
    /// <summary>
    /// Implements Employee data service
    /// </summary>
    public class EmployeeDataService : CoreDataService,IEmployeeDataService
    {
        #region Public Methods
        /// <summary>
        /// Get all employees from DB
        /// </summary>
        /// <returns></returns>
        public List<Employees> GetEmployees()
        {
            List<Employees> employees;
            using (var connection = new SqlConnection(base.ConnectionString))
            {
                connection.Open();
                employees = connection.Query<Employees>("Select * from Employees").ToList();
                connection.Close();
            }
            return employees;
        } 
        #endregion
    }
}
